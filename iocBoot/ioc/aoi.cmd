
# N = AOI index 10 .. n
# C = analog input channel index as data source 0 .. 9
# ACQ_SAMPLES = maximum number of samples in this AOI
dbLoadRecords("sis8300fcA.template",     "P=$(PREFIX),R=AOI$(N):,   PORT=$(PORT), ADDR=$(N), TIMEOUT=1,CHANNEL=$(C),NAME=AOI$(N),NELEMENTS=$(ACQ_SAMPLES),NTSPOINTS=1000,RST=ST$(N):")

# expose AOI to clients
NDStdArraysConfigure("AOI$(N)", 3, 0, "$(PORT)", $(N))
dbLoadRecords("NDStdArrays.template",    "P=$(PREFIX),R=AOI$(N):,   PORT=AOI$(N), ADDR=0,    TIMEOUT=1,NDARRAY_PORT=$(PORT),NDARRAY_ADDR=$(N),TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(ACQ_SAMPLES)")
