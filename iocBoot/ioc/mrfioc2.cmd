###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
# Timing MTCA EVR 300u
# As per EVR MTCA 300 engineering manual ch 5.3.5
# Example found in https://github.com/icshwi/e3-mrfioc2/blob/master/cmds/MTCA-EVR-300_standalone_template.cmd

# machine event clock, EVR expects MHz
epicsEnvSet("EVENT_CLOCK"       "88.0525")

# see devlib2/pciApp/devLibPCI.c, devPCIFindSpec(): D:B.F, with digits in HEX!
# NOTE: we can use 1st column of lspci output directly
mrmEvrSetupPCI("$(EVR_UNIT)", "0b:00.0")

# Needed with software timestamp source w/o RT thread scheduling ###
var evrMrmTimeNSOverflowThreshold 100000

# mTCA-EVR-300, the normal one, with the micro-SCSI connector (16 FPUV outputs)
#dbLoadRecords("evr-mtca-300-ess.db",   "SYS=$(LOC):PBI, D=$(EVR_UNIT), EVR=$(EVR_UNIT), P=$(LOC):PBI, FEVT=$(EVENT_CLOCK)")
# mTCA-EVR-300U, which replaces the micro-SCSI connector for two slots for MRF universal modules (4 FPUV outputs)
dbLoadRecords("evr-mtca-300u-ess.db",  "SYS=$(LOC):PBI, D=$(EVR_UNIT), EVR=$(EVR_UNIT), P=$(LOC):PBI, FEVT=$(EVENT_CLOCK)")

# TODO: it would be nice to have this
# set_requestfile_path("$(MRFIOC2)/req")

# END of mrfioc2.cmd
###############################################################################
