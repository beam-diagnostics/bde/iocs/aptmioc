###############################################################################
#
#  Development Aperture monitor system
#
###############################################################################

< envPaths

# SYSTEM: BD system that this IOC controls
epicsEnvSet("SYSTEM",                       "BD-APTM1")
# LOC: short name used in PV names
epicsEnvSet("LOC",                          "APTM1")
epicsEnvSet("ACQ_UNIT1",                    "AMC1")
epicsEnvSet("ACQ_UNIT2",                    "AMC2")
epicsEnvSet("ACQ_UNIT3",                    "AMC3")
# EVR has a bit different requirements on split LOC:UNIT
epicsEnvSet("EVR_UNIT",                     "EVR1")

# ACQ_SAMPLES: maximum number of samples to acquire
#              100k per channel per rate (14 Hz), this is highly conservative
#              when looking at ~10Msps for 6ms of pulse length.
epicsEnvSet("ACQ_SAMPLES",                  "100000")
# HOLD_SAMPLES: maximum number of timeseries data
#               6 pulses, make sure to follow ACQ_SAMPLES changes
epicsEnvSet("HOLD_SAMPLES",                 "600000")
# TREND_SAMPLES: maximum number of pulse statistics in trending PVs
epicsEnvSet("TREND_SAMPLES",                "1000")

# EPICS_CA_MAX_ARRAY_BYTES: 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
# EPICS_DB_INCLUDE_PATH: list all module db folders
epicsEnvSet("EPICS_DB_INCLUDE_PATH",        "$(TOP)/db:$(ADCORE)/db:$(ADPICO8)/db:$(MRFIOC2)/db")
# limit CAS to controls network only
# not sure if it helps?!
# epicsEnvSet("EPICS_CAS_INTF_ADDR_LIST",     "enp0s25")
# epicsEnvSet("EPICS_CAS_IGNORE_ADDR_LIST",   "192.168.1.25")

< main.cmd
