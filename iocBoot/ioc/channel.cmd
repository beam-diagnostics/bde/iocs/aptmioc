
# C = analog input channel index 0 .. 9
# ACQ_SAMPLES = maximum number of samples in this C

dbLoadRecords("sis8300fcN.template",       "P=$(PREFIX),R=CH$(C):,   PORT=$(PORT), ADDR=$(C), TIMEOUT=1, NAME=CH$(C)")

# we can add StdArray for analog input if we like
# NDStdArraysConfigure("CH$(C)", 3, 0, "$(PORT)", $(C))
# dbLoadRecords("NDStdArrays.template",    "P=$(PREFIX),R=CH$(C):,   PORT=CH$(C),  ADDR=0,    TIMEOUT=1, NDARRAY_PORT=$(PORT),NDARRAY_ADDR=$(C), TYPE=Float64,FTVL=DOUBLE,NELEMENTS=$(ACQ_SAMPLES)")

# Some example DB commands
# NDTimeSeriesConfigure("TS$(N)", $(QSIZE), 0, "$(PORT)", $(N), 1)
# dbLoadRecords("NDTimeSeries.template",   "P=$(PREFIX),R=TS$(N):,   PORT=TS$(N),  ADDR=0,    TIMEOUT=1, NDARRAY_PORT=$(PORT),NDARRAY_ADDR=$(N),NCHANS=$(TSPOINTS),TIME_LINK=,ENABLED=0")
# dbLoadRecords("NDTimeSeriesN.template",  "P=$(PREFIX),R=TS$(N):,   PORT=TS$(N),  ADDR=0,    TIMEOUT=1, NAME=TS$(N), NCHANS=$(TSPOINTS)")

# NDROIConfigure("ROI$(N)", $(QSIZE), 0, "$(PORT)", $(N), 0, 0, 0, 0, $(MAX_THREADS=4))
# dbLoadRecords("NDROI.template",          "P=$(PREFIX),R=ROI$(N):,  PORT=ROI$(N), ADDR=0,    TIMEOUT=1, NDARRAY_PORT=$(PORT),NDARRAY_ADDR=$(N)")

# NDStatsConfigure("ST$(N)", $(QSIZE), 0, "ROI$(N)", 0, 0, 0, 0, 0, $(MAX_THREADS=4))
# dbLoadRecords("NDStats.template",        "P=$(PREFIX),R=ST$(N):,   PORT=ST$(N),  ADDR=0,    TIMEOUT=1, NDARRAY_PORT=ROI$(N),NDARRAY_ADDR=0,HIST_SIZE=256,XSIZE=$(XSIZE),YSIZE=$(YSIZE),NCHANS=$(NCHANS)")

# NDROIStatConfigure("RS$(N)", $(QSIZE), 0, "$(PORT)", $(N), 1, 0, 0, 0, 0, $(MAX_THREADS=4))
# dbLoadRecords("NDROIStat.template",      "P=$(PREFIX),R=RS$(N):,   PORT=RS$(N),  ADDR=0,    TIMEOUT=1, NDARRAY_PORT=$(PORT),NDARRAY_ADDR=$(N),NCHANS=$(NCHANS)")
# dbLoadRecords("NDROIStatN.template",     "P=$(PREFIX),R=RS$(N):1:, PORT=RS$(N),  ADDR=0,    TIMEOUT=1, NCHANS=$(NCHANS)")
