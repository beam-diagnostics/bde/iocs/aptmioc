
###
### This is clunky, but mrfioc2 does not have autosave request files set up
###

# Example found in https://github.com/icshwi/e3-mrfioc2/blob/master/cmds/MTCA-EVR-300_standalone_template.cmd

# event at 14 Hz (machine rate), EVR expects integer
epicsEnvSet("TRIG_EVT_CODE"         "14")
# pulse in LEBT is ~6 ms, EVR expects us
epicsEnvSet("PULSE_WIDTH"           "6000")
# event to trigger delay, EVR expects us
epicsEnvSet("PULSE_DELAY"           "100")

### Get current time from system clock ###
dbpf $(LOC):PBI-$(EVR_UNIT):TimeSrc-Sel 2

### Set up the prescaler that will trigger the sequencer at 14 Hz ###
# in standalone mode because real freq from synthsiezer is 88.05194802 MHz, otherwise 6289464
# Event rate = clock rate / prescaler
# 14 Hz ~= 88052500 / 6289464
# 1 Hz  ~= 88052500 / 88052500
dbpf $(LOC):PBI-$(EVR_UNIT):PS0-Div-SP 6289424

### Set up the sequencer ###
# normal mode, rearm after finish
dbpf $(LOC):PBI-$(EVR_UNIT):SoftSeq0-RunMode-Sel 0
# trigger the sequence from prescaler 0
dbpf $(LOC):PBI-$(EVR_UNIT):SoftSeq0-TrigSrc:2-Sel 2
# select us as the units of the timestamp array of the seuqncer
dbpf $(LOC):PBI-$(EVR_UNIT):SoftSeq0-TsResolution-Sel 2
dbpf $(LOC):PBI-$(EVR_UNIT):SoftSeq0-Load-Cmd 1
dbpf $(LOC):PBI-$(EVR_UNIT):SoftSeq0-Enable-Cmd 1

### Trigger backplane trigger line X at 14 Hz ###
dbpf $(LOC):PBI-$(EVR_UNIT):DlyGen0-Evt-Trig0-SP $(TRIG_EVT_CODE)
# trigger pulse width in us resolution (see SoftSeq0-TsResolution-Sel)
dbpf $(LOC):PBI-$(EVR_UNIT):DlyGen0-Width-SP $(PULSE_WIDTH)
# trigger from delay generator 0
dbpf $(LOC):PBI-$(EVR_UNIT):DlyGen0-Delay-SP $(PULSE_DELAY)

### Trigger backplane trigger line X at 14 Hz ###
# backplane line X; X=0..7
# dbpf $(LOC):PBI-$(EVR_UNIT):OutBackX-Src-SP 0
# backplane 0
dbpf $(LOC):PBI-$(EVR_UNIT):OutBack0-Src-SP 0

### Send event clock over backplane clock line TCLKB, MCH should be configured
# to send the clock back to the AMCs over TCLKA ###
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Src-SP 63
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Ena-Sel 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pwr-Sel 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.BF 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.BE 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.BD 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.BC 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.BB 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.BA 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B9 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B8 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B7 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B6 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B5 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B4 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B3 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B2 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B1 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low00_15-SP.B0 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low16_31-SP.BF 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low16_31-SP.BE 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low16_31-SP.BD 1
dbpf $(LOC):PBI-$(EVR_UNIT):OutTCLKB-Pat:Low16_31-SP.BC 1

### Run the script that configures the events and timestamp of the sequence ###
#system("/bin/sh ./configure_sequencer_14Hz.sh $(LOC):PBI $(EVR_UNIT)")
# XXX: can not use dbpf since it can not handle arrays of numbers..
system "caput -a $(LOC):PBI-$(EVR_UNIT):SoftSeq0-EvtCode-SP 2 14 127 >/dev/null"
system "caput -a $(LOC):PBI-$(EVR_UNIT):SoftSeq0-Timestamp-SP 2 0 1 >/dev/null"
system "caput    $(LOC):PBI-$(EVR_UNIT):SoftSeq0-Commit-Cmd 1 >/dev/null"
