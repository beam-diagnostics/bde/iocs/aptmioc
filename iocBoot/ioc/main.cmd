###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
#  Aperture monitor system

errlogInit(20000)

dbLoadDatabase("$(TOP)/dbd/aptmApp.dbd")
aptmApp_registerRecordDeviceDriver(pdbbase)

# we need caput and caRepeater
epicsEnvSet(PATH, $(PATH):$(EPICS_BASE)/bin/linux-x86_64)

# digitizer support
< adpico8.cmd

# timing
# < mrfioc2.cmd

###############################################################################

iocInit()

###############################################################################

# save things every thirty seconds
# create_monitor_set("auto_settings.req", 30, "LOC=$(LOC),ACQ1=$(ACQ_UNIT1),ACQ2=$(ACQ_UNIT2),ACQ3=$(ACQ_UNIT3),EVR=$(EVR_UNIT)")

###############################################################################

# < post_init.cmd

###############################################################################
