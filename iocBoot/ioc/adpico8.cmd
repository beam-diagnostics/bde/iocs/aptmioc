###############################################################################
########    this snippet needs to be included in top level st.cmd     #########
#
# DAMC Pico8 based digitizer system for Aperture monitor

epicsEnvSet("PICODEV1",      "/dev/amc_pico_0000:05:00.0")
epicsEnvSet("PICODEV2",      "/dev/amc_pico_0000:07:00.0")
epicsEnvSet("PICODEV3",      "/dev/amc_pico_0000:08:00.0")
epicsEnvSet("PREFIX1",       "$(LOC):")
epicsEnvSet("PREFIX2",       "$(LOC):")
epicsEnvSet("PREFIX3",       "$(LOC):")
epicsEnvSet("PORT1",         "APTM1")
epicsEnvSet("PORT2",         "APTM2")
epicsEnvSet("PORT3",         "APTM3")

# number of areas of interest to instantiate
epicsEnvSet("NUM_AOI",       "2")

epicsEnvSet("XSIZE",         "$(ACQ_SAMPLES)")
epicsEnvSet("YSIZE",         "1")
epicsEnvSet("TSPOINTS",      "$(HOLD_SAMPLES)")
epicsEnvSet("QSIZE",         "20")
epicsEnvSet("NCHANS",        "64")
epicsEnvSet("CBUFFS",        "500")
epicsEnvSet("MAX_THREADS",   "4")


# Create a pico8 driver
# pico8Configure(const char *portName, const char *devicePath, 
#             int maxAOI, int numSamples, int dataType,
#             int maxBuffers, int maxMemory,
#             int priority, int stackSize)
#dataType Initial data type of the detector data. These are the enum values for NDDataType_t, i.e.
#    0=NDInt8
#    1=NDUInt8
#    2=NDInt16
#    3=NDUInt16
#    4=NDInt32
#    5=NDUInt32
#    6=NDFloat32
#    7=NDFloat64
# sis8300fcConfigure("$(PORT)", "$(SISDEV)", $(NUM_AOI), $(ACQ_SAMPLES), 7, 0, 0)
# dbLoadRecords("sis8300fc.template",    "P=$(PREFIX),R=,    PORT=$(PORT),ADDR=0,TIMEOUT=1,MAX_SAMPLES=$(ACQ_SAMPLES)")
pico8Configure("$(PORT1)", "$(PICODEV1)", $(NUM_AOI), $(ACQ_SAMPLES), 7, 0, 0)
dbLoadRecords("pico8.template",        "P=$(PREFIX1),R=$(ACQ_UNIT1):,   PORT=$(PORT1),ADDR=0,TIMEOUT=1")
dbLoadRecords("pico8N.template",       "P=$(PREFIX1),R=$(ACQ_UNIT1):1:, PORT=$(PORT1),ADDR=0,TIMEOUT=1,NAME=CH1")
dbLoadRecords("pico8N.template",       "P=$(PREFIX1),R=$(ACQ_UNIT1):2:, PORT=$(PORT1),ADDR=1,TIMEOUT=1,NAME=CH2")

pico8Configure("$(PORT2)", "$(PICODEV2)", $(NUM_AOI), $(ACQ_SAMPLES), 7, 0, 0)
dbLoadRecords("pico8.template",        "P=$(PREFIX2),R=$(ACQ_UNIT2):,   PORT=$(PORT2),ADDR=0,TIMEOUT=1")

pico8Configure("$(PORT3)", "$(PICODEV3)", $(NUM_AOI), $(ACQ_SAMPLES), 7, 0, 0)
dbLoadRecords("pico8.template",        "P=$(PREFIX3),R=$(ACQ_UNIT3):,   PORT=$(PORT3),ADDR=0,TIMEOUT=1")

# XXX: figure out how to use channels/AOI with three AMC?!

# # Load channel 0 .. 9 support
# epicsEnvSet("C",     "0")
# < channel.cmd

# # Load AOI 10 for channel 0 support
# epicsEnvSet("C",     "0")
# epicsEnvSet("N",     "10")
# < aoi.cmd
# # Load AOI 11 for channel 0 support
# epicsEnvSet("C",     "0")
# epicsEnvSet("N",     "11")
# < aoi.cmd

# XXX: figure out how to use autosave with three AMC, each with different PREFIX?!

# set_requestfile_path("./")
# set_requestfile_path("$(ADPICO8)/req")
# set_requestfile_path("$(ADCORE)/req")
# set_requestfile_path("$(BUSY)/req")
# set_requestfile_path("$(SSCAN)/req")
# set_requestfile_path("$(CALC)/req")
# set_savefile_path("./autosave")
# set_pass0_restoreFile("auto_settings.sav")
# set_pass1_restoreFile("auto_settings.sav")
# save_restoreSet_status_prefix("$(PREFIX)")
# dbLoadRecords("$(AUTOSAVE)/db/save_restoreStatus.db", "P=$(PREFIX)")

# asynSetTraceIOMask("$(PORT1)",0,2)
# asynSetTraceMask("$(PORT1)",0,255)

# END of adpico8.cmd
###############################################################################
