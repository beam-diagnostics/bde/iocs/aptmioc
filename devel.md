# development setup info

# CPU

    [dev@isclab-willie ~]$ ifconfig
    enp0s25: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
            inet 10.0.4.114  netmask 255.255.252.0  broadcast 10.0.7.255
            inet6 fe80::8b1e:d454:7bed:dc25  prefixlen 64  scopeid 0x20<link>
            ether 00:40:9e:04:a7:ea  txqueuelen 1000  (Ethernet)
            RX packets 282655  bytes 115228725 (109.8 MiB)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 12082  bytes 1411273 (1.3 MiB)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
            device interrupt 20  memory 0xc0a00000-c0a20000

    enp18s0f0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
            inet6 fe80::236:b812:da56:d631  prefixlen 64  scopeid 0x20<link>
            ether 00:40:9e:04:a7:eb  txqueuelen 1000  (Ethernet)
            RX packets 21  bytes 1260 (1.2 KiB)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 216  bytes 37968 (37.0 KiB)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
            device memory 0xc0520000-c053ffff

    enp18s0f1: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
            inet 192.168.1.25  netmask 255.255.255.0  broadcast 192.168.1.255
            inet6 fe80::39df:f566:7daa:299  prefixlen 64  scopeid 0x20<link>
            ether 00:40:9e:04:a7:ec  txqueuelen 1000  (Ethernet)
            RX packets 255  bytes 49023 (47.8 KiB)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 418  bytes 63820 (62.3 KiB)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
            device memory 0xc0500000-c051ffff

    enp19s0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
            ether 00:40:9e:04:a7:ed  txqueuelen 1000  (Ethernet)
            RX packets 869581  bytes 55747120 (53.1 MiB)
            RX errors 0  dropped 0  overruns 0  frame 0
            TX packets 869581  bytes 55747120 (53.1 MiB)
            TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
            device interrupt 16  memory 0xc0400000-c0420000

    ...

# MCH

    [dev@isclab-willie ~]$ telnet 192.168.1.41
    Trying 192.168.1.41...
    Connected to 192.168.1.41.
    Escape character is '^]'.

     Welcome to N.A.T. MCH V2.19.3 Final (r14670) (Sep 18 2017 - 13:47)


     Current open telnet sessions:
       192.168.1.25:44688
       192.168.1.25:44694 (this connection)

    Type <?> to see a list of available commands.
    nat>
    nat> show_fru

    FRU Information:
    ----------------
     FRU  Device   State  Name
    ==========================================
      0   MCH       M4    NMCH-CM
      3   mcmc1     M4    NAT-MCH-MCMC
      6   AMC2      M4    CCT AM 900/412
      9   AMC5      M4    SIS8300KU AMC
     10   AMC6      M4    mTCA-EVR-300
     40   CU1       M4    Schroff uTCA CU
     50   PM1       M4    NAT-PM-AC600D
     60   Clock1    M4    MCH-Clock
     61   HubMod1   M4    MCH-PCIe
     94   AMC5-RTM  M4    SIS8300KU RTM
    ==========================================

# Ethercat

See [ecat.md] for more info.

# Timing

See [timing.md] for more info.
