# Timing

## IOC startup 300

    ###############################################################################
    ########    this snippet needs to be included in top level st.cmd     #########
    #
    # Timing MTCA EVR 300u
    # As per EVR MTCA 300 engineering manual ch 5.3.5
    # see devlib2/pciApp/devLibPCI.c, devPCIFindSpec(): D:B.F, with digits in HEX!
    # NOTE: we can use 1st column of lspci output directly
    mrmEvrSetupPCI("PBI-EVR-030", "0b:00.0")
    Notice: devPCIFindSpec() expect B:D.F in hex
    Device PBI-EVR-030  11:0.0 slot=6
    Using IRQ 71
    FWVersion 0x18070207
    Found version 519
    Found SFP EEPROM
    Sequencer capability detected
    mTCA: Out FP:4 FPUNIV:0 RB:16 IFP:2 GPIO:0
    EVR FIFO task start
    Enabling interrupts
    # Q: which one do we have in FC?
    dbLoadRecords("/home/dev/bde/R3.15.5/mrfioc2-2.2.0/db/evr-mtca-300-ess.db",  "SYS=FEB-030Row, D=PBI-EVR-030, EVR=PBI-EVR-030, P=FEB-030Row, Link-Clk-SP=88.0525")
    # dbLoadRecords("$(MRFIOC2)/db/evr-mtca-300u-ess.db", "D=$(EVR_DEVICE), SYS=$(EVR_SYS), EVR=$(EVR_DEVICE), P=$(EVR_SYS), Link-Clk-SP=88.0525")
    # are events and pulsers already in the above DB file? see substitutions..
    # dbLoadRecords("$(MRFIOC2)/db/evr-softEvent.template", "D=$(DEVICE), SYS=$(SYS), EVT=14, CODE=14")
    # dbLoadRecords("$(MRFIOC2)/db/evr-pulserMap.template", "D=$(DEVICE), SYS=$(SYS), PID=0, F=Trig, ID=0, EVT=14")
    # TODO: do we need to configure event 14 here?
    #       what about pulsar?
    # TODO: it would be nice to have this
    # set_requestfile_path("$(MRFIOC2)/evrMrmApp/Db")
    # END of mrfioc2.cmd
    ###############################################################################
    # ethercat
    #< ethercat.cmd
    ###############################################################################
    iocInit()
    Starting iocInit
    ############################################################################
    ## EPICS R3.15.5
    ## EPICS Base built Jun  8 2018
    ############################################################################
    FEB-030Row-PBI-EVR-030:OutFPUV02-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut2' : Object not found : PBI-EVR-030:FrontUnivOut2
    FEB-030Row-PBI-EVR-030:OutFPUV03-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut3' : Object not found : PBI-EVR-030:FrontUnivOut3
    FEB-030Row-PBI-EVR-030:OutFPUV04-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut4' : Object not found : PBI-EVR-030:FrontUnivOut4
    FEB-030Row-PBI-EVR-030:OutFPUV05-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut5' : Object not found : PBI-EVR-030:FrontUnivOut5
    FEB-030Row-PBI-EVR-030:OutFPUV06-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut6' : Object not found : PBI-EVR-030:FrontUnivOut6
    FEB-030Row-PBI-EVR-030:OutFPUV07-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut7' : Object not found : PBI-EVR-030:FrontUnivOut7
    FEB-030Row-PBI-EVR-030:OutFPUV08-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut8' : Object not found : PBI-EVR-030:FrontUnivOut8
    FEB-030Row-PBI-EVR-030:OutFPUV09-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut9' : Object not found : PBI-EVR-030:FrontUnivOut9
    FEB-030Row-PBI-EVR-030:OutFPUV10-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut10' : Object not found : PBI-EVR-030:FrontUnivOut10
    FEB-030Row-PBI-EVR-030:OutFPUV11-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut11' : Object not found : PBI-EVR-030:FrontUnivOut11
    FEB-030Row-PBI-EVR-030:OutFPUV12-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut12' : Object not found : PBI-EVR-030:FrontUnivOut12
    FEB-030Row-PBI-EVR-030:OutFPUV13-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut13' : Object not found : PBI-EVR-030:FrontUnivOut13
    FEB-030Row-PBI-EVR-030:OutFPUV14-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut14' : Object not found : PBI-EVR-030:FrontUnivOut14

    ...

## IOC startup 300u

    ###############################################################################
    ########    this snippet needs to be included in top level st.cmd     #########
    #
    # Timing MTCA EVR 300u
    # As per EVR MTCA 300 engineering manual ch 5.3.5
    # see devlib2/pciApp/devLibPCI.c, devPCIFindSpec(): D:B.F, with digits in HEX!
    # NOTE: we can use 1st column of lspci output directly
    mrmEvrSetupPCI("PBI-EVR-030", "0b:00.0")
    Notice: devPCIFindSpec() expect B:D.F in hex
    Device PBI-EVR-030  11:0.0 slot=6
    Using IRQ 71
    FWVersion 0x18070207
    Found version 519
    Found SFP EEPROM
    Sequencer capability detected
    mTCA: Out FP:4 FPUNIV:0 RB:16 IFP:2 GPIO:0
    EVR FIFO task start
    Enabling interrupts
    # Q: which one do we have in FC?
    #dbLoadRecords("$(MRFIOC2)/db/evr-mtca-300-ess.db",  "SYS=$(LOC), D=$(EVR_UNIT), EVR=$(EVR_UNIT), P=$(LOC), Link-Clk-SP=88.0525")
    dbLoadRecords("/home/dev/bde/R3.15.5/mrfioc2-2.2.0/db/evr-mtca-300u-ess.db",  "SYS=FEB-030Row, D=PBI-EVR-030, EVR=PBI-EVR-030, P=FEB-030Row, Link-Clk-SP=88.0525")
    # are events and pulsers already in the above DB file? see substitutions..
    # dbLoadRecords("$(MRFIOC2)/db/evr-softEvent.template", "D=$(DEVICE), SYS=$(SYS), EVT=14, CODE=14")
    # dbLoadRecords("$(MRFIOC2)/db/evr-pulserMap.template", "D=$(DEVICE), SYS=$(SYS), PID=0, F=Trig, ID=0, EVT=14")
    # TODO: do we need to configure event 14 here?
    #       what about pulsar?
    # TODO: it would be nice to have this
    # set_requestfile_path("$(MRFIOC2)/evrMrmApp/Db")
    # END of mrfioc2.cmd
    ###############################################################################
    # ethercat
    #< ethercat.cmd
    ###############################################################################
    iocInit()
    Starting iocInit
    ############################################################################
    ## EPICS R3.15.5
    ## EPICS Base built Jun  8 2018
    ############################################################################
    FEB-030Row-PBI-EVR-030:OutFPUV2-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut2' : Object not found : PBI-EVR-030:FrontUnivOut2
    FEB-030Row-PBI-EVR-030:OutFPUV3-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut3' : Object not found : PBI-EVR-030:FrontUnivOut3
    FEB-030Row-PBI-EVR-030:OutTCLKA-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut16' : Object not found : PBI-EVR-030:FrontUnivOut16
    FEB-030Row-PBI-EVR-030:OutTCLKB-Ena-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut17' : Object not found : PBI-EVR-030:FrontUnivOut17
    FEB-030Row-PBI-EVR-030:OutFPUV2-Src-RB_: failed to find/create object 'PBI-EVR-030:FrontUnivOut2' : Object not found : PBI-EVR-030:FrontUnivOut2
    FEB-030Row-PBI-EVR-030:OutFPUV2-Src2-RB: failed to find/create object 'PBI-EVR-030:FrontUnivOut2' : Object not found : PBI-EVR-030:FrontUnivOut2
    FEB-030Row-PBI-EVR-030:OutFPUV3-Src-RB_: failed to find/create object 'PBI-EVR-030:FrontUnivOut3' : Object not found : PBI-EVR-030:FrontUnivOut3
    FEB-030Row-PBI-EVR-030:OutFPUV3-Src2-RB: failed to find/create object 'PBI-EVR-030:FrontUnivOut3' : Object not found : PBI-EVR-030:FrontUnivOut3
    FEB-030Row-PBI-EVR-030:OutTCLKA-Src-RB_: failed to find/create object 'PBI-EVR-030:FrontUnivOut16' : Object not found : PBI-EVR-030:FrontUnivOut16
    FEB-030Row-PBI-EVR-030:OutTCLKA-Src2-RB: failed to find/create object 'PBI-EVR-030:FrontUnivOut16' : Object not found : PBI-EVR-030:FrontUnivOut16
    FEB-030Row-PBI-EVR-030:OutTCLKB-Src-RB_: failed to find/create object 'PBI-EVR-030:FrontUnivOut17' : Object not found : PBI-EVR-030:FrontUnivOut17
    FEB-030Row-PBI-EVR-030:OutTCLKB-Src2-RB: failed to find/create object 'PBI-EVR-030:FrontUnivOut17' : Object not found : PBI-EVR-030:FrontUnivOut17
    FEB-030Row-PBI-EVR-030:OutFPUV2-Src-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut2' : Object not found : PBI-EVR-030:FrontUnivOut2
    FEB-030Row-PBI-EVR-030:OutFPUV2-Src2-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut2' : Object not found : PBI-EVR-030:FrontUnivOut2
    FEB-030Row-PBI-EVR-030:OutFPUV3-Src-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut3' : Object not found : PBI-EVR-030:FrontUnivOut3
    FEB-030Row-PBI-EVR-030:OutFPUV3-Src2-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut3' : Object not found : PBI-EVR-030:FrontUnivOut3
    FEB-030Row-PBI-EVR-030:OutTCLKA-Src-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut16' : Object not found : PBI-EVR-030:FrontUnivOut16
    FEB-030Row-PBI-EVR-030:OutTCLKA-Src2-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut16' : Object not found : PBI-EVR-030:FrontUnivOut16
    FEB-030Row-PBI-EVR-030:OutTCLKB-Src-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut17' : Object not found : PBI-EVR-030:FrontUnivOut17
    FEB-030Row-PBI-EVR-030:OutTCLKB-Src2-SP: failed to find/create object 'PBI-EVR-030:FrontUnivOut17' : Object not found : PBI-EVR-030:FrontUnivOut17
    Set EVR clock 124916000.000000
    iocRun: All initialization complete
    ###############################################################################
    # save things every thirty seconds
