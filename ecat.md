# Ethercat

## drivers

    [root@isclab-willie dev]# modprobe ec_master main_devices="00:40:9e:04:a7:ed"
    [root@isclab-willie dev]# modprobe ec_generic


    [root@isclab-willie dev]# dmesg
    ...
    [  724.248614] EtherCAT: Master module cleaned up.
    [  764.637560] EtherCAT: Master driver 1.5.2 unknown
    [  764.637851] EtherCAT: 1 master waiting for devices.
    [  777.509843] ec_generic: EtherCAT master generic Ethernet device module 1.5.2 unknown
    [  777.509886] EtherCAT: Accepting 00:40:9E:04:A7:ED as main device for master 0.
    [  777.509906] ec_generic: Binding socket to interface 3 (enp19s0).
    [  777.516083] EtherCAT 0: Starting EtherCAT-IDLE thread.
    [  777.516275] EtherCAT 0: Link state of ecm0 changed to UP.
    [  777.517067] EtherCAT 0: 6 slave(s) responding on main device.
    [  777.517089] EtherCAT 0: Slave states on main device: INIT.
    [  777.517706] EtherCAT 0: Scanning bus.
    [  778.295727] EtherCAT 0: Bus scanning completed in 778 ms.
    [  778.295730] EtherCAT 0: Using slave 0 as DC reference clock.
    [  778.297003] EtherCAT 0: Slave states on main device: INIT, PREOP.
    [  778.317968] EtherCAT 0: Slave states on main device: PREOP.

## master

    [dev@isclab-willie ~]$ ethercat master
    Master0
      Phase: Idle
      Active: no
      Slaves: 6
      Ethernet devices:
        Main: 00:40:9e:04:a7:ed (attached)
          Link: UP
          Tx frames:   908707
          Tx bytes:    54616356
          Rx frames:   908706
          Rx bytes:    54616296
          Tx errors:   0
          Tx frame rate [1/s]:    996    996    996
          Tx rate [KByte/s]:     58.4   58.4   58.3
          Rx frame rate [1/s]:    996    996    996
          Rx rate [KByte/s]:     58.4   58.4   58.3
        Common:
          Tx frames:   908707
          Tx bytes:    54616356
          Rx frames:   908706
          Rx bytes:    54616296
          Lost frames: 0
          Tx frame rate [1/s]:    996    996    996
          Tx rate [KByte/s]:     58.4   58.4   58.3
          Rx frame rate [1/s]:    996    996    996
          Rx rate [KByte/s]:     58.4   58.4   58.3
          Loss rate [1/s]:          0     -0      0
          Frame loss [%]:         0.0   -0.0    0.0
      Distributed clocks:
        Reference clock: Slave 0
        Application time: 0
                          2000-01-01 00:00:00.000000000

## slaves

    [dev@isclab-willie ~]$ ethercat slaves
    0  0:0  PREOP  +  EK1100 EtherCAT-Koppler (2A E-Bus)
    1  0:1  PREOP  +  EL3602 2K. Ana. Eingang +/- 10Volt, Diff. 24bit
    2  0:2  PREOP  +  EL4134 4K. Ana. Ausgang -10/+10V. 16bit
    3  0:3  PREOP  +  EL2124 4K. Dig. Ausgang 5V, 20mA
    4  0:4  PREOP  +  EL2004 4K. Dig. Ausgang 24V, 0.5A
    5  0:5  PREOP  +  EL1014 4K. Dig. Eingang 24V, 10�s

### slave 1

    [dev@isclab-willie bde]$ ethercat -p 1 pdos
    SM0: PhysAddr 0x1000, DefaultSize  128, ControlRegister 0x26, Enable 1
    SM1: PhysAddr 0x1080, DefaultSize  128, ControlRegister 0x22, Enable 1
    SM2: PhysAddr 0x1100, DefaultSize    0, ControlRegister 0x04, Enable 0
    SM3: PhysAddr 0x1180, DefaultSize   12, ControlRegister 0x20, Enable 1
      TxPDO 0x1a00 "AI TxPDO-Map Inputs Ch.1"
        PDO entry 0x6000:01,  1 bit, "Underrange"
        PDO entry 0x6000:02,  1 bit, "Overrange"
        PDO entry 0x6000:03,  2 bit, "Limit 1"
        PDO entry 0x6000:05,  2 bit, "Limit 2"
        PDO entry 0x6000:07,  1 bit, "Error"
        PDO entry 0x0000:00,  7 bit, "Gap"
        PDO entry 0x1800:07,  1 bit, ""
        PDO entry 0x1800:09,  1 bit, ""
        PDO entry 0x6000:11, 32 bit, "Value"
      TxPDO 0x1a01 "AI TxPDO-Map Inputs Ch.2"
        PDO entry 0x6010:01,  1 bit, "Underrange"
        PDO entry 0x6010:02,  1 bit, "Overrange"
        PDO entry 0x6010:03,  2 bit, "Limit 1"
        PDO entry 0x6010:05,  2 bit, "Limit 2"
        PDO entry 0x6010:07,  1 bit, "Error"
        PDO entry 0x0000:00,  7 bit, "Gap"
        PDO entry 0x1801:07,  1 bit, ""
        PDO entry 0x1801:09,  1 bit, ""
        PDO entry 0x6010:11, 32 bit, "Value"

### slave 2

    [dev@isclab-willie bde]$ ethercat -p 2 pdos
    SM0: PhysAddr 0x1000, DefaultSize  128, ControlRegister 0x26, Enable 1
    SM1: PhysAddr 0x1080, DefaultSize  128, ControlRegister 0x22, Enable 1
    SM2: PhysAddr 0x1100, DefaultSize    8, ControlRegister 0x24, Enable 1
      RxPDO 0x1600 "AO RxPDO-Map OutputsCh.1"
        PDO entry 0x7000:01, 16 bit, "Analog output"
      RxPDO 0x1601 "AO RxPDO-Map OutputsCh.2"
        PDO entry 0x7010:01, 16 bit, "Analog output"
      RxPDO 0x1602 "AO RxPDO-Map OutputsCh.3"
        PDO entry 0x7020:01, 16 bit, "Analog output"
      RxPDO 0x1603 "AO RxPDO-Map OutputsCh.4"
        PDO entry 0x7030:01, 16 bit, "Analog output"
    SM3: PhysAddr 0x1180, DefaultSize    0, ControlRegister 0x00, Enable 0

### slave 3

    [dev@isclab-willie bde]$ ethercat -p 3 pdos
    SM0: PhysAddr 0x0f00, DefaultSize    0, ControlRegister 0x44, Enable 9
      RxPDO 0x1600 "Channel 1"
        PDO entry 0x7000:01,  1 bit, "Output"
      RxPDO 0x1601 "Channel 2"
        PDO entry 0x7010:01,  1 bit, "Output"
      RxPDO 0x1602 "Channel 3"
        PDO entry 0x7020:01,  1 bit, "Output"
      RxPDO 0x1603 "Channel 4"
        PDO entry 0x7030:01,  1 bit, "Output"

### slave 4

    [dev@isclab-willie bde]$ ethercat -p 4 pdos
    SM0: PhysAddr 0x0f00, DefaultSize    0, ControlRegister 0x44, Enable 9
      RxPDO 0x1600 "Channel 1"
        PDO entry 0x7000:01,  1 bit, "Output"
      RxPDO 0x1601 "Channel 2"
        PDO entry 0x7010:01,  1 bit, "Output"
      RxPDO 0x1602 "Channel 3"
        PDO entry 0x7020:01,  1 bit, "Output"
      RxPDO 0x1603 "Channel 4"
        PDO entry 0x7030:01,  1 bit, "Output"

### slave 5

    [dev@isclab-willie bde]$ ethercat -p 5 pdos
    SM0: PhysAddr 0x1000, DefaultSize    1, ControlRegister 0x00, Enable 1
      TxPDO 0x1a00 "Channel 1"
        PDO entry 0x6000:01,  1 bit, "Input"
      TxPDO 0x1a01 "Channel 2"
        PDO entry 0x6010:01,  1 bit, "Input"
      TxPDO 0x1a02 "Channel 3"
        PDO entry 0x6020:01,  1 bit, "Input"
      TxPDO 0x1a03 "Channel 4"
        PDO entry 0x6030:01,  1 bit, "Input"

## IOC startup

    ###############################################################################
    ########    this snippet needs to be included in top level st.cmd     #########
    #
    # Ethercat I/O for motion and HV control
    # epicsEnvSet("ECAT2_PREFIX",   "$(DEVICE)-ECAT2")
    # epicsEnvSet("HV_IO_PREFIX",     "$(LOC_PREFIX):PBI-ECATIO-083")
    # epicsEnvSet("MC_IO_PREFIX",     "$(LOC_PREFIX):PBI-ECATIO-081")
    # epicsEnvSet("FREQ",             "$(FREQ=1000)")
    ecat2configure(0, 1000, 1, 1)
    ===== ecat2: Requesting master...succeeded
    ===== ecat2: Configuring EL6692 entries start...
    ===== ecat2: Configuring EL6692 entries end.
    ===== ecat2: Slaves added: 6
    ===== ecat2: Configuring slave(s) start...
    ===== ecat2: Configuring slave(s) end.
    ===== ecat2: add_domain: Domain 0 creation succeeded
    ===== ecat2: domain_create_autoconfig: Autoconfiguring domain...
    ===== ecat2: Domain 0: Autoconfig found 32 entries
    ===== ecat2:
    ===== ecat2: Domain 0
    ===== ecat2: Domain frequency 1000.0 Hz (1 network packet sent every 1.000000 ms)
    ===== ecat2: Domain registers 32
    ===== ecat2: Domain size 23 bytes (allocated 4096 bytes)
    ===== ecat2: Slave-to-slave entries 0
    ===== ecat2: Domain 0 configure process completed
    ##############################  TODO fix ECATIO indexes !!!
    # analog input (-10/+10 V)
    # AI1: voltage monitor
    # AI2: current limit monitor
    dbLoadRecords("ecat2el3602.db", "PREFIX=FEB-030Row, MOD_ID=PBI-ECATIO-111, SLAVE_IDX=1")
    # analog output (-10/+10 V)
    # AO1: voltage set
    # AO2: current limit set
    # AO3: ??
    # AO4: ??
    dbLoadRecords("ecat2el4134.db", "PREFIX=FEB-030Row, MOD_ID=PBI-ECATIO-222, SLAVE_IDX=2")
    # digital output 1 (0/5 V)
    # DO1: HV on/off set
    # DO2: polarity neg/pos set
    # DO3: ??
    # DO4: ??
    dbLoadRecords("ecat2el2124.db", "PREFIX=FEB-030Row, MOD_ID=PBI-ECATIO-333, SLAVE_IDX=3")
    # digital output 2 (0/24 V)
    # DO1: move cup
    # DO2: set polarity
    # DO3: ??
    # DO4: ??
    dbLoadRecords("ecat2el2004.db", "PREFIX=FEB-030Row, MOD_ID=PBI-ECATIO-444, SLAVE_IDX=4")
    # digital input (0/24 V)
    # DI1: limit switch in
    # DI2: limit switch out
    # DI3: ??
    # DI4: ??
    dbLoadRecords("ecat2el1014.db", "PREFIX=FEB-030Row, MOD_ID=PBI-ECATIO-555, SLAVE_IDX=5")
    # FC specific records
    dbLoadRecords("/home/dev/bde/R3.15.5/fcioc-R1-0/db/fc_ecat_hv.template",          "P=FEB-030Row:, R=PBI-HV-006, MOD_P=FEB-030Row:, MOD_DO0=PBI-ECATIO-333, MOD_AI0=PBI-ECATIO-111, MOD_AO0=PBI-ECATIO-222")
    dbLoadRecords("/home/dev/bde/R3.15.5/fcioc-R1-0/db/fc_ecat_motion.template",      "P=FEB-030Row:, R=PBI-MC-006, MOD_P=FEB-030Row:, MOD_DO0=PBI-ECATIO-444, MOD_DI0=PBI-ECATIO-555")
    # Water flow status Not in release 1.0
    # Temperature status Not in release 1.0
    # TODO: it would be nice to have this at least for output records?
    #       OTOH, maybe it is better to have some defaults elsewhere; post iocInit()?
    # set_requestfile_path("$(ECAT2DB)/ecat2dbApp/Db")
    # END of ethercat.cmd
    ###############################################################################
    ###############################################################################
    iocInit()
    Starting iocInit
    ############################################################################
    ## EPICS R3.15.5
    ## EPICS Base built Jun  8 2018
    ############################################################################
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-333-DO1 (bo) as d0.r20 (s3.sm0.p0.e0), offset 20.0 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-333-DO2 (bo) as d0.r21 (s3.sm0.p1.e0), offset 20.1 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-333-DO3 (bo) as d0.r22 (s3.sm0.p2.e0), offset 20.2 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-333-DO4 (bo) as d0.r23 (s3.sm0.p3.e0), offset 20.3 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-444-DO1 (bo) as d0.r24 (s4.sm0.p0.e0), offset 21.0 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-444-DO2 (bo) as d0.r25 (s4.sm0.p1.e0), offset 21.1 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-444-DO3 (bo) as d0.r26 (s4.sm0.p2.e0), offset 21.2 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-444-DO4 (bo) as d0.r27 (s4.sm0.p3.e0), offset 21.3 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-222-AO1 (ao) as d0.r16 (s2.sm2.p0.e0), offset 12.0 (16 bits)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-222-AO2 (ao) as d0.r17 (s2.sm2.p1.e0), offset 14.0 (16 bits)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-222-AO3 (ao) as d0.r18 (s2.sm2.p2.e0), offset 16.0 (16 bits)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-222-AO4 (ao) as d0.r19 (s2.sm2.p3.e0), offset 18.0 (16 bits)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI1-LIM1 (longin) as d0.r2 (s1.sm3.p0.e2), offset 0.2 (2 bits)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI1-LIM2 (longin) as d0.r3 (s1.sm3.p0.e3), offset 0.4 (2 bits)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI2-LIM1 (longin) as d0.r10 (s1.sm3.p1.e2), offset 6.2 (2 bits)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI2-LIM2 (longin) as d0.r11 (s1.sm3.p1.e3), offset 6.4 (2 bits)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-STATUS (bi) as STATUS_SLAVE_OP (m0.s1) system record
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI1-URNG (bi) as d0.r0 (s1.sm3.p0.e0), offset 0.0 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI1-ORNG (bi) as d0.r1 (s1.sm3.p0.e1), offset 0.1 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI1-ERR (bi) as d0.r4 (s1.sm3.p0.e4), offset 0.6 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI1-TXPST (bi) as d0.r5 (s1.sm3.p0.e6), offset 1.6 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI1-TXPTOG (bi) as d0.r6 (s1.sm3.p0.e7), offset 1.7 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI2-URNG (bi) as d0.r8 (s1.sm3.p1.e0), offset 6.0 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI2-ORNG (bi) as d0.r9 (s1.sm3.p1.e1), offset 6.1 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI2-ERR (bi) as d0.r12 (s1.sm3.p1.e4), offset 6.6 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI2-TXPST (bi) as d0.r13 (s1.sm3.p1.e6), offset 7.6 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI2-TXPTOG (bi) as d0.r14 (s1.sm3.p1.e7), offset 7.7 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-222-STATUS (bi) as STATUS_SLAVE_OP (m0.s2) system record
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-333-STATUS (bi) as STATUS_SLAVE_OP (m0.s3) system record
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-444-STATUS (bi) as STATUS_SLAVE_OP (m0.s4) system record
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-555-STATUS (bi) as STATUS_SLAVE_OP (m0.s5) system record
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-555-DI1 (bi) as d0.r28 (s5.sm0.p0.e0), offset 22.0 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-555-DI2 (bi) as d0.r29 (s5.sm0.p1.e0), offset 22.1 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-555-DI3 (bi) as d0.r30 (s5.sm0.p2.e0), offset 22.2 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-555-DI4 (bi) as d0.r31 (s5.sm0.p3.e0), offset 22.3 (1 bit)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI1 (ai) as d0.r7 (s1.sm3.p0.e8), offset 2.0 (32 bits)
    ===== ecat2: configured record FEB-030Row:PBI-ECATIO-111-AI2 (ai) as d0.r15 (s1.sm3.p1.e8), offset 8.0 (32 bits)
    Record FEB-030Row:PBI-ECATIO-111-AI1-LIM1 registered as I/O Intr, offset 0000, bit 2, bitlen 4, 1 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI1-LIM2 registered as I/O Intr, offset 0000, bit 4, bitlen 4, 2 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI2-LIM1 registered as I/O Intr, offset 0006, bit 2, bitlen 4, 3 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI2-LIM2 registered as I/O Intr, offset 0006, bit 4, bitlen 4, 4 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-STATUS registered as I/O Intr (EtherCAT system record), 5 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI1-URNG registered as I/O Intr, offset 0000, bit 0, bitlen 1, 6 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI1-ORNG registered as I/O Intr, offset 0000, bit 1, bitlen 1, 7 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI1-ERR registered as I/O Intr, offset 0000, bit 6, bitlen 1, 8 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI1-TXPST registered as I/O Intr, offset 0001, bit 6, bitlen 1, 9 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI1-TXPTOG registered as I/O Intr, offset 0001, bit 7, bitlen 1, 10 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI2-URNG registered as I/O Intr, offset 0006, bit 0, bitlen 1, 11 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI2-ORNG registered as I/O Intr, offset 0006, bit 1, bitlen 1, 12 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI2-ERR registered as I/O Intr, offset 0006, bit 6, bitlen 1, 13 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI2-TXPST registered as I/O Intr, offset 0007, bit 6, bitlen 1, 14 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI2-TXPTOG registered as I/O Intr, offset 0007, bit 7, bitlen 1, 15 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-222-STATUS registered as I/O Intr (EtherCAT system record), 16 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-333-STATUS registered as I/O Intr (EtherCAT system record), 17 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-444-STATUS registered as I/O Intr (EtherCAT system record), 18 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-555-STATUS registered as I/O Intr (EtherCAT system record), 19 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-555-DI1 registered as I/O Intr, offset 0016, bit 0, bitlen 1, 20 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-555-DI2 registered as I/O Intr, offset 0016, bit 1, bitlen 1, 21 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-555-DI3 registered as I/O Intr, offset 0016, bit 2, bitlen 1, 22 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-555-DI4 registered as I/O Intr, offset 0016, bit 3, bitlen 1, 23 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI1 registered as I/O Intr, offset 0002, bit 0, bitlen 4, 24 I/O Intr records total so far
    Record FEB-030Row:PBI-ECATIO-111-AI2 registered as I/O Intr, offset 0008, bit 0, bitlen 4, 25 I/O Intr records total so far
    ===== ecat2: worker, irq and slave-check threads started
    iocRun: All initialization complete
    ###############################################################################
    # save things every thirty seconds
    # SIS8300, AD, AD plugins
    create_monitor_set("auto_settings_adsis8300.req", 30, "P=FEB-030Row:PBI-AMC-031")
    save_restore:readReqFile: unable to open file auto_settings_adsis8300.req. Exiting.
    # Timing?
    # create_monitor_set("auto_settings_mrfioc2.req", 30, "P=$(EVR_PREFIX)")
    # Ethercat I/O HV ?
    # create_monitor_set("auto_settings_ethercat.req", 30, "P=$(HV_PREFIX)")
    # Ethercat I/O other ?
    # create_monitor_set("auto_settings_ethercat.req", 30, "P=$(IO_PREFIX)")
